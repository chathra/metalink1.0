<!-- top header -->
<?php
ob_start();
require_once('admin/category.php');
$cat = Category::find_all();
?>
<?php
require 'view/top-header.php';
require_once('admin/category.php');
?>

<body class="cnt-home">
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">
    <!-- top header content -->
    <?php
    require 'view/top-header-content.php';
    // menu PC
    require 'view/menu-pc.php';
    ?>
    <!-- ============================================== NAVBAR ============================================== -->

    <!-- /.container-class -->


    <!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
            <ul class="list-inline list-unstyled">
                <li><a href="#">Home</a></li>
                <li class='active'>Smart Phone</li>
            </ul>
        </div>
        <!-- /.breadcrumb-inner -->
    </div>
    <!-- /.container -->
</div>

<!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
    <div class='container'>
        <div class='row outer-bottom-sm'>
            <div class='col-md-3 sidebar'>
                <?php
                require_once('admin/category.php');
                require_once('admin/item.php');
                $cat = Category::find_all();
                ?>

                <!-- ================================== TOP NAVIGATION ================================== -->
                <?php
                require 'view/main-category-panel.php'
                ?>
                <!-- /.side-menu -->
                <!-- ================================== TOP NAVIGATION : END ================================== -->
                <div class="sidebar-module-container">
                    <h3 class="section-title">shop by</h3>


                    <div class="sidebar-filter">
                        <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                        <?php

                        require 'view/category-item-view.php';
                        ?>
                        <!-- /.sidebar-widget -->
                        <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->
                        <!-- ============================================== MANUFACTURES============================================== -->
                        <?php
                        require 'view/manufactures.php';
                        ?>
                        <!-- /.sidebar-widget -->
                    </div>
                    <!-- /.sidebar-filter -->
                </div>
                <!-- /.sidebar-module-container -->
            </div>
            <!-- /.sidebar -->
            <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">

                <div id="hero" class="homepage-slider3">
                    <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                        <div class="full-width-slider">
                            <div class="item" style="background-image: url(assets/images/sliders/fur1.jpg);">
                                <div class="container-fluid">
                                    <div class="caption vertical-center text-left">
                                        <div class="small fadeInDown-1">
                                            italia trend
                                        </div>

                                        <div class="big-text fadeInDown-2">
                                            open minded design
                                        </div>

                                        <div class="excerpt fadeInDown-3 hidden-xs">created for customers with
                                            modern,active and easy lifestyles ,this collection features sleek,innovative
                                            designs envisioned in the style center of italy.
                                        </div>
                                    </div>
                                    <!-- /.caption -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>
                            <!-- /.item -->
                        </div>
                        <!-- /.full-width-slider -->

                        <div class="full-width-slider">
                            <div class="item full-width-slider"
                                 style="background-image: url(assets/images/sliders/fur1.jpg);">
                                <div class="container-fluid">
                                    <div class="caption vertical-center text-left">
                                        <div class="small fadeInDown-1">
                                            italia trend
                                        </div>

                                        <div class="big-text fadeInDown-2">
                                            open minded design
                                        </div>

                                        <div class="excerpt fadeInDown-3 hidden-xs">created for customers with
                                            modern,active and easy lifestyles ,this collection features sleek,innovative
                                            designs envisioned in the style center of italy.
                                        </div>
                                    </div>
                                    <!-- /.caption -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>
                            <!-- /.item -->
                        </div>
                        <!-- /.full-width-slider -->

                    </div>
                    <!-- /.owl-carousel -->
                </div>

                <!-- Best offer -->
                <?php
                require 'view/best-offer-banner3x3.php';
                ?>

                <!-- ========================================= SECTION – HERO : END ========================================= -->
                <div class="clearfix filters-container m-t-10">
                    <div class="row">
                        <div class="col col-sm-6 col-md-2">
                            <div class="filter-tabs">
                                <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                                    <li class="active">
                                        <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-list"></i>Grid</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.filter-tabs -->
                        </div>
                        <!-- /.col -->

                        <!-- /.col -->

                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <div class="search-result-container">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active " id="grid-container">
                            <div class="category-product  inner-top-vs">
                                <div class="row">
                                    <div class="products">
                                        <!-- item -->
                                        <?php
                                        require 'view/items.php';?>

                                    </div>
                                </div>
                                <!-- /.category-product -->
                            </div>
                            <!-- /.tab-pane -->
                        </div>


                        <!-- /.tab-content -->
                        <div class="clearfix filters-container">

                            <div class="text-right">
                                <div class="pagination-container">
                                    <ul class="list-inline list-unstyled">
                                       <?php for($number=1;$number<=$pages;$number++)
                                        { //echo "<a href=?page=$number>".$number."</a>";

                                        echo "<li ><a href=?page=$number>".$number."</a></li>";

                                        }
                                        ?>
                                    </ul>
                                    <!-- /.list-inline -->
                                </div>
                                <!-- /.pagination-container -->                            </div>
                            <!-- /.text-right -->

                        </div>
                        <!-- /.filters-container -->

                    </div>
                </div>
                <!-- /.logo-slider -->
                <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
            </div>
            <!-- /.container -->

        </div>

    </div>


    <!-- /.body-content -->
    <!-- ============================================================= FOOTER ============================================================= -->
    <footer id="footer" class="footer color-bg">
        <?php
        require 'view/footer.php';
        ?>
    </footer>
    <!-- ============================================================= FOOTER : END============================================================= -->

    <!-- include customer css -->
    <?php
    include 'view/script.php';
    ?>
    <script type="text/javascript">
        $(".navbar-nav li").removeClass("active");
        jQuery('#indexTab').addClass('active');
    </script>

</body>

<!-- Mirrored from transvelo.in/demo/html/unicase/index.php?page=category by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 31 May 2015 17:02:34 GMT -->
</html>



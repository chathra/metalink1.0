<section class="section">
<h3 class="section-title">new arrivals</h3>

<div class="owl-carousel homepage-owl-carousel custom-carousel outer-top-xs owl-theme" data-item="2">

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/6.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag new"><span>new</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/5.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag sale"><span>sale</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/3.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag sale"><span>sale</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/2.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/4.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/1.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag hot"><span>hot</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->
</div>
</section>
<?php
require_once('admin/category.php');
require_once('admin/item.php');
$cat = Category::find_all();

?>

<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
    <nav  role="navigation">
        <ul class="nav ">
            <?php foreach ($cat as $c): ?>
                <li>
                    <a href="all-item-view.php?code=<?php echo $c->catgry_code; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="icon fa fa-cube fa-fw"></i>
                        <?php
                        $item_rows = Item::get_num_row('item_cat', $c->ctgry_id);
                        echo $c->catgry_name ." (".$item_rows .")";
                        ?>
                    </a>
                </li>
            <?php endforeach; ?>
            <!-- /.menu-item -->
        </ul>
        <!-- /.nav -->
    </nav>
    <!-- /.megamenu-horizontal -->
</div>
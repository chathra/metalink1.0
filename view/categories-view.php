<section class="section featured-product inner-xs wow fadeInUp">
<h3 class="section-title">Featured products</h3>

<div class="owl-carousel best-seller custom-carousel owl-theme outer-top-xs">

<div class="item">
    <div class="products">
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s1.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s1.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s1.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s1.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
    </div>
</div>

<div class="item">
    <div class="products">
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s2.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s2.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s2.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s2.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
    </div>
</div>

<div class="item">
    <div class="products">
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s3.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s3.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s3.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s3.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
    </div>
</div>

<div class="item">
    <div class="products">
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s2.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s2.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s2.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s2.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
    </div>
</div>

<div class="item">
    <div class="products">
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s1.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s1.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s1.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s1.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
    </div>
</div>

<div class="item">
    <div class="products">
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s3.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s3.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
        <div class="product">
            <div class="product-micro">
                <div class="row product-micro-row">
                    <div class="col col-xs-6">
                        <div class="product-image">
                            <div class="image">
                                <a href="assets/images/furniture-products/s3.jpg" data-lightbox="image-1"
                                   data-title="Nunc ullamcors">
                                    <img data-echo="assets/images/furniture-products/s3.jpg"
                                         src="assets/images/blank.gif" width="170" height="174" alt="">

                                    <div class="zoom-overlay"></div>
                                </a>
                            </div>
                            <!-- /.image -->

                        </div>
                        <!-- /.product-image -->
                    </div>
                    <!-- /.col -->
                    <div class="col col-xs-6">
                        <div class="product-info">
                            <h3 class="name"><a href="#">Simple Product Demo</a></h3>

                            <div class="rating rateit-small"></div>
                            <div class="product-price">
												<span class="price">
													$650.99
												</span>

                            </div>
                            <!-- /.product-price -->
                            <div class="action"><a href="#" class="lnk btn btn-primary">Add To Cart</a></div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.product-micro-row -->
            </div>
            <!-- /.product-micro -->
        </div>
    </div>
</div>
</div>
</section>
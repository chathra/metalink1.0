
<?php
require_once('admin/category.php');
require_once('admin/item.php');
require_once('admin/image.php');
require_once('admin/initialize.php');
require_once('admin/database.php');




//mysql_connect('localhost','root','admin');
//mysql_select_db('metalinklive');
$per_page=5;
$pages_query=$db->query("select count(item_id) from itemdetails");
$pages=ceil(mysql_result($pages_query,0)/$per_page);
ob_start();
if(!isset($_GET['page']))
{
    header("location:index.php?page=1");
}else
{
    $page=$_GET['page'];
}

$start=(($page-1)*$per_page);
$get_items=$db->query("select * from itemdetails limit $start,$per_page");


while($row=mysql_fetch_assoc($get_items)){
    $catname=Category::find_by_id('catgry_code',$row['item_cat']);
    //print_r($catname->catgry_name);
    //echo 'img/' .$catname->catgry_name.'/'. $row['item_code'].'.jpg';
    ?>
    <div class="col-sm-6 col-md-4 wow fadeInUp">
        <div class="products">

            <div class="product">
                <div class="product-image">
                    <div class="image">
                        <a href="item-view.php?name=<?php echo $row['item_name']; ?>&id=<?php echo $row['item_code']; ?>&cat=<?php echo $catname->catgry_name; ?>">
                            <img src="<?php
                            if (!file_exists('img/' .$catname->catgry_code.'/'. $row['item_code'].'.jpg')) {
                                echo "not found";
                            } else {
                                echo 'img/' .$catname->catgry_code.'/'. $row['item_code'].'.jpg';
                            }
                            $image = null;
                            ?>  " width="200" height="200"/>
                        </a>
                    </div>
                    <!-- /.image -->
                </div>
                <!-- /.product-image -->

                <div class="product-info text-left">
                    <h3 class="name"><a
                            href="item-view.php?name=<?php echo $row['item_name']; ?>&id=<?php echo $row['item_code']; ?>&cat=<?php echo $catname->catgry_name; ?>">
                            <?php
                            echo $row['item_name'];  ?>
                        </a></h3>

                    <div class="rating rateit-small"></div>
                    <div class="description"></div>
                    <div class="product-price">
				<span class="price">Rs
                    <?php
                    echo number_format($row['item_price'], 2);
                    ?>
				</span>
                        <span class="price-before-discount">$ 800</span>
                    </div>
                    <!-- /.product-price -->
                </div>
                <!-- /.product-info -->
                <div class="cart clearfix animate-effect">
                    <div class="action">
                        <ul class="list-unstyled">
                            <a href="item-view.php?name=<?php echo $row['item_name'];; ?>&id=<?php echo $row['item_code']; ?>&cat=<?php echo $catname->catgry_name; ?>"
                            <li class="add-cart-button btn-group">
                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                    <i class="fa fa-shopping-cart"></i>
                                </button>
                                <button class="btn btn-primary" type="button">View More</button>
                            </li>
                            </a>
                        </ul>
                    </div>
                    <!-- /.action -->
                </div>
                <!-- /.cart -->
            </div>
            <!-- /.product -->
        </div>
    </div>
    <!-- /.products -->

<?php }?>

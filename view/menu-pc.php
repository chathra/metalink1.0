<?php
require_once('admin/category.php');
$cat = Category::find_all();
?>
<div class="main-header ">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                <!-- ============================================================= LOGO ============================================================= -->
                <div class="logo">
                    <a href="index9ba3.html?page=home">
                        <img src="assets/images/logo.png" alt="">
                    </a>
                </div>
                <!-- /.logo -->
                <!-- ============================================================= LOGO : END ============================================================= -->
            </div>
            <!-- /.logo-holder -->

            <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
                <div class="contact-row">
                    <div class="phone inline">
                        <i class="icon fa fa-phone"></i> (+94) 773429342
                    </div>
                    <div class="contact inline">
                        <i class="icon fa fa-envelope"></i> info@themetalink.com
                    </div>
                </div>
                <!-- /.contact-row -->


                <!-- ============================================================= SEARCH AREA ============================================================= -->
                <div class="search-area">
                    <form>
                        <div class="control-group">

                            <ul class="categories-filter animate-dropdown">
                                <li class="dropdown">

                                    <a class="dropdown-toggle"  data-toggle="dropdown" href="indexe2f2.html?page=category">Select  Category <b class="caret"></b></a>

                                    <ul class="dropdown-menu" role="menu" >
<?php
                                         foreach($cat as $c):
                                    ?>
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="404.php"><?php echo $c->catgry_name; ?><</a></li>
                                       <?php endforeach;?>
                                    </ul>
                                </li>
                            </ul>

                            <input class="search-field" placeholder="Search here..." />

                            <a class="search-button" href="#" ></a>

                        </div>
                    </form>
                </div>
                <!-- /.search-area -->
                <!-- ============================================================= SEARCH AREA : END ============================================================= -->
            </div>
            <!-- /.top-search-holder -->

            <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
                <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->

                <div class="dropdown dropdown-cart">
                    <a href="404.php" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
                        <div class="items-cart-inner">
                            <div class="total-price-basket">
                                <span class="lbl">cart -</span>
					<span class="total-price">
						<span class="sign">Rs</span>
						<span class="value">0</span>
					</span>
                            </div>
                            <div class="basket">
                                <i class="glyphicon glyphicon-shopping-cart"></i>
                            </div>
                            <div class="basket-item-count"><span class="count">0</span></div>

                        </div>
                    </a>
                </div>
                <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
            </div>
        </div>
    </div>

</div>

<!-- menu -->
<div class="header-nav animate-dropdown ">
    <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse"
                        class="navbar-toggle collapsed"
                        type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="nav-bg-class">
                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                    <div class="nav-outer">
                        <ul class="nav navbar-nav">
                            <li class="active" id="indexTab">
                                <a href="index.php">Home</a>
                            </li>
                            <li class="" id="aboutTab">
                                <a href="contact-us.php">About US</a>
                            </li>
                            <li class="" id="contactTab">
                                <a href="contact-us.php">Contact</a>
                            </li>
                        </ul>
                        <!-- /.navbar-nav -->
                        <div class="clearfix"></div>
                    </div>
                    <!-- /.nav-outer -->
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.nav-bg-class -->
        </div>
        <!-- /.navbar-default -->
    </div>
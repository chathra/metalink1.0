<?php
require_once('admin/category.php');
require_once('admin/item.php');
$cat = Category::find_all();

?>
<div class="sidebar-widget wow fadeInUp outer-bottom-xs ">
    <div class="widget-header m-t-20">
        <h4 class="widget-title">Category</h4>
    </div>
    <div class="sidebar-widget-body m-t-10">
        <div class="accordion">
            <?php foreach ($cat as $c): ?>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <?php
                        // view main category item
                        $item_rows = Item::get_num_row('item_cat', $c->ctgry_id);
                        $collapse = ($item_rows > 0) ? 'collapse' : null;
                        ?>
                        <a href="#<?php echo $c->ctgry_id ?>" data-toggle="<?php echo $collapse; ?>"
                           class="accordion-toggle collapsed">
                            <?php
                            echo $c->catgry_name;
                            ?>
                        </a>
                    </div>

                    <?php
                    if ($item_rows > 0) {
                        //view category has sub item
                        $sql="select item_id,item_name,item_cat,item_code from itemdetails where item_cat= " . $c->ctgry_id;
                        $item_array = Item::find_by_sql($sql);
                        ?>
                        <div class="accordion-body collapse" id="<?php echo $c->ctgry_id ?>" style="height: 0px;">
                            <div class="accordion-inner">
                                <ul>
                                    <?php foreach ($item_array as $item): ?>
                                        <li>
                                            <a
                                                href="item-view.php?cat=<?php echo $c->catgry_code ?>&id=<?php echo $item->item_id ?>&name=<?php echo $item->item_name ?>">
                                                <?php echo $item->item_name;  ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <!-- /.accordion-inner -->
                        </div>
                    <?php
                    }
                    ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <!-- /.sidebar-widget-body -->
</div>
<div class="col-md-5">
    <div class="wide-banner cnt-strip">
        <div class="image">
            <img width="470" height="146" alt="" src="assets/images/banners/fr1.jpg">
        </div>
        <div class="strip">
            <div class="strip-inner">
                <h3>new trend</h3>

                <h2>for new room</h2>
            </div>
        </div>
    </div>
    <!-- /.wide-banner -->
</div>
<!-- /.col -->

<div class="col-md-7">
    <div class="wide-banner cnt-strip">
        <div class="image">
            <img width="670" height="146" alt="" src="assets/images/banners/fr2.jpg">
        </div>
        <div class="strip">
            <div class="strip-inner text-right" style="display:inline-block;">
                <h2>
                    one stop place for
                    <br>
                    <span class="shopping-needs">all your shopping needs</span>
                </h2>
            </div>
        </div>
    </div>
    <!-- /.wide-banner -->
</div>
<?php
require_once('admin/item.php');
$sql = "select distinct item_manufacture from itemdetails where item_state=1";
$item_array = Item::find_by_sql($sql);
?>

<div class="sidebar-widget outer-bottom-xs wow fadeInUp">
    <div class="widget-header">
        <h4 class="widget-title">Manufactures</h4>
    </div>

        <div class="sidebar-widget-body m-t-10">
            <ul class="list">
                <?php
                foreach ($item_array as $item):
                ?>
                <li><a href="404.php"><?php echo $item->item_manufacture; ?></a></li>
                <?php
                endforeach;
                ?>
            </ul>
            <!--<a href="#" class="lnk btn btn-primary">Show Now</a>-->
        </div>

    <!-- /.sidebar-widget-body -->
</div>
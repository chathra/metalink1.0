<?php
/**
 * Created by PhpStorm.
 * User: {}
 * Date: 5/18/2015
 * Time: 4:39 PM
 */
require_once('initialize.php');
require_once('database.php');
class Image extends Db_object{

    public $image_id;
    public $item_code;
    public $image_name;
    public $image_date;
    protected static $table='itemimage';
    public  static $db_fields=array('item_code','image_name','image_date');

    function __construct()
    {
        parent::$id=$this->image_id;
        $this->image_date=strftime("%Y-%m-%d %H:%M:%S",time());


    }

    public function upload($file)
        {
            $file_name=$file['name'];
            $file_tmp=$file['tmp_name'];
            $file_size=$file['size'];
            $file_error=$file['error'];





            $file_ext=explode('.',$file_name );
        $file_ext=strtolower(end($file_ext));
        $allowed=array('jpg');
        $new_name=$this->image_name.".jpg";
        $category=Item::find_by_id('item_code',$this->item_code);


        if (in_array($file_ext,$allowed))
        {
            if ($file_error===0)
            {
                if(!is_dir("../img/". $category->item_cat ."/")) {
                    mkdir("../img/". $category->item_cat ."/");
                }
                $file_dest="../img/". $category->item_cat."/".$new_name;
                if(move_uploaded_file($file_tmp,$file_dest))
                {
                    echo 'success';
                    if(self::find_by_id('image_name',$this->image_name))
                    {
                        self::update('item_code',$this->item_code);
                    }else{
                        self::create();

                    }
                }
            }
        }
    }



}
$img1=new Image();
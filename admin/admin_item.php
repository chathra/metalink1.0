<?php
require 'view/header.php';
require('initialize.php');
require_once('image.php');

$item1=new Item();
$cat_list=Category::find_all();


if(isset($_GET['itm']))
{$form_title="Update Item";
    $t=$_GET['itm'];
    $subname='update';
    echo 'get get';

    $itm= Item::find_by_id('item_code',$t);
    $itmid=$itm->item_id;
    $itmname=$itm->item_name;
    $itmcode=$itm->item_code;
    $itmcat=$itm->item_cat;
    $catname=Category::find_by_id('catgry_code',$itmcat);
    $itmmodelno=$itm->item_modelNo;
    $itmprocvty=$itm->item_prdtivity;
    $itmsize=$itm->item_size;
    $itmcompny=$itm->item_company;
    $itmmanufactr=$itm->item_manufacture;
    $itmdescrip=$itm->item_desc;
    $itmprice=$itm->item_price;

}
else {
    $form_title="Create Item";
    $itmcat="";
    $itmname ="";
    $itmcode= "";
    $itmmodelno = "";
    $itmprocvty="";
    $itmsize = "";
    $itmcompny= "";
    $itmmanufactr="";
    $itmdescrip="";
    $itmprice= "";
    $subname='submit';

}
if(isset($_POST['update'])) {
    $item1->item_id=trim($_POST['item_id']);
    $itmcat=$item1->item_cat=trim($_POST['itmcat']);
    $itmname = $item1->item_name=trim($_POST['itmname']);
    $itmcode=$item1->item_code= trim($_POST['itmcode']);
    $itmmodelno =$item1->item_modelNo= trim($_POST['itmmodelno']);
    $itmprocvty= $item1->item_prdtivity=trim($_POST['itmprocvty']);
    $itmsize = $item1->item_size=trim($_POST['itmsize']);
    $itmcompny= $item1->item_company=trim($_POST['itmcompny']);
    $itmmanufactr=$item1->item_manufacture= trim($_POST['itmmanufactr']);
    $itmdescrip= $item1->item_desc=trim($_POST['itmdescrip']);
    $itmprice= $item1->item_price=trim($_POST['itmprice']);
    $item1->item_lstUpdate=strftime("%Y-%m-%d %H:%M:%S",time());

    $item1->update('item_code',$itmcode);


    echo "<script type='text/javascript'> alert('Item updated successfully'); </script>";

    redirect_to('admin_image.php?id='.$itmcode);
}
elseif (isset($_POST['submit'])) {

    $itmcat=$item1->item_cat=trim($_POST['itmcat']);
    $itmname = $item1->item_name=trim($_POST['itmname']);
    $itmcode=$item1->item_code= trim($_POST['itmcode']);
    $itmmodelno =$item1->item_modelNo= trim($_POST['itmmodelno']);
    $itmprocvty= $item1->item_prdtivity=trim($_POST['itmprocvty']);
    $itmsize = $item1->item_size=trim($_POST['itmsize']);
    $itmcompny= $item1->item_company=trim($_POST['itmcompny']);
    $itmmanufactr=$item1->item_manufacture= trim($_POST['itmmanufactr']);
    $itmdescrip= $item1->item_desc=trim($_POST['itmdescrip']);
    $itmprice= $item1->item_price=trim($_POST['itmprice']);
    $item1->item_date=strftime("%Y-%m-%d %H:%M:%S",time());
    if($item1->is_exist($itmcode))
    {
        echo "<script type='text/javascript'> alert('Item code exists enter different code !'); </script>";
    }else{
        $item1->create();
        echo "<script type='text/javascript'> alert('Item saved successfully '); </script>";
        redirect_to('admin_image.php?id='.$itmcode);;}
}

?>

<body>
<?php
require 'view/menu-pc.php';

?>

<div class="container-fluid fixed">


    <div id="content">
        <ul class="breadcrumb">
            <li><a href="index.html" class="glyphicons home"><i></i> AIR</a></li>
            <li class="divider"></li>
            <li>Forms</li>
            <li class="divider"></li>
            <li>Demo Forms</li>
            <div class="widget widget-2 widget-tabs widget-tabs-2 tabs-right border-bottom-none">
                <div class="widget-head">
                    <ul>
                        <li class="active"><a class="glyphicons settings" href="#account-settings"
                                              data-toggle="tab"><i></i><?php echo $form_title;?></a></li>

                    </ul>
                </div>
            </div>

            <form class="form-horizontal" method="post" action="admin_item.php"/>
            <div class="tab-pane active" id="account-settings">
                <div class="row-fluid">
                    <div class="span3">
                        <strong>Add New Item</strong>
                        <p class="muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="span9">
                        <div class="separator"></div>

                        <label for="inputPasswordOld">Main Category</label><select class="span10" name="itmcat">
                            <optgroup label="Picnic">
                                <?php foreach($cat_list as $ct) {
                                    if(isset($_GET['itm'])){
                                        echo  "<option value=".$catname->catgry_code.">".$catname->catgry_name;
                                        break;
                                    }
                                    echo  "<option value=".$ct->catgry_code.">".$ct->catgry_name;}?>
                            </optgroup>
                        </select>
                        <!-- item name -->
                        <label for="inputPasswordOld">Item Name</label>
                        <input type="text" id="inputPasswordOld" class="span10" value="<?php echo $itmname;?>"
                               placeholder="Leave empty for no change" name="itmname"/>
                        <input type="hidden" name="item_id" value="<?php echo $itmid;?>">
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>
                        <!-- item code -->
                        <label for="inputPasswordOld">Item Code</label>
                        <input type="text" id="inputPasswordOld" class="span10" value="<?php echo $itmcode?>"
                               placeholder="Leave empty for no change" name="itmcode"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>

                        <label for="inputPasswordOld">Model NO</label>
                        <input type="text" name="itmmodelno" id="inputPasswordOld" class="span10" value="<?php echo $itmmodelno?>"
                               placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>

                        <label for="inputPasswordOld">Productivity</label>
                        <input type="text" name="itmprocvty" id="inputPasswordOld" class="span10" value="<?php echo $itmprocvty?>"
                               placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>

                        <label for="inputPasswordOld">Size</label>
                        <input type="text" name="itmsize" id="inputPasswordOld" class="span10" value="<?php echo $itmsize?>"
                               placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>

                        <label for="inputPasswordOld">Company</label>
                        <input type="text" name="itmcompny" id="inputPasswordOld" class="span10" value="<?php echo $itmcompny ?>"
                               placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>

                        <label for="inputPasswordOld">Manufactures</label>
                        <input type="text" name="itmmanufactr" id="inputPasswordOld" class="span10" value="<?php echo $itmmanufactr?>"
                               placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>
                        <div class="separator"></div>

                        <label for="inputPasswordOld">Description</label>
                        <input type="text" name="itmdescrip" id="inputPasswordOld" class="span10" value="<?php echo $itmdescrip ?>"
                               placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>

                        <label for="inputPasswordOld">Price</label>
                        <input type="text" name="itmprice" id="inputPasswordOld" class="span10" value="<?php echo $itmprice?>"
                               placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                        <div class="separator"></div>



                    </div>
                </div>
                <hr class="separator line"/>
            </div>
            <div class="form-actions" style="margin: 0; padding-right: 0;">
                <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok pull-right" name="<?php echo $subname?>" value=""><i></i>Save
                    changes
                </button>
            </div>

    </div>
</div>
</form>
</div>
<?php

require 'view/footer.php';
?>

</body>
</html>

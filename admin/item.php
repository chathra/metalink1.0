<?php
/**
 * Created by PhpStorm.
 * User: {}
 * Date: 5/25/2015
 * Time: 5:33 PM
 */
require_once('db_object.php');
class Item extends Db_object{
    public $item_id;
    public $item_name;
    public $item_cat;
    public $item_code;
    public $item_modelNo;
    public $item_prdtivity;
    public $item_size;
    public $item_company;
    public $item_desc;
    public $item_manufacture;
    public $item_price;
    public $item_lstUpdate;
    public $item_date;
    public $item_state;
    protected static $table='itemdetails';
    public  static $db_fields=array('item_id','item_name','item_cat','item_code','item_modelNo','item_prdtivity','item_size','item_company','item_desc','item_manufacture','item_price','item_lstUpdate','item_date','item_state');

    function __construct()
    {
        // $this->item_lstUpdate='';
        //$this->item_date=strftime("%Y-%m-%d %H:%M:%S",time());
        $this->item_state=1;
        $this->item_id='';
    }

    public static function is_exist($itmcode)
    {
        $result_array=self::find_by_sql("select * from itemdetails WHERE  item_code='$itmcode'");
        return !empty($result_array)?array_shift($result_array):false;
    }

}
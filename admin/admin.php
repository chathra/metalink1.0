<?php

require 'view/header.php';
require_once('initialize.php');



//mysql_connect('localhost','root','admin');
//mysql_select_db('metalinklive');
$per_page=5;
$pages_query=$db->query("select count(item_id) from itemdetails");
$pages=ceil(mysql_result($pages_query,0)/$per_page);

if(!isset($_GET['page']))
{
    header("location:admin.php?page=1");
}else
{
    $page=$_GET['page'];
}

$start=(($page-1)*$per_page);
$colors=$db->query("select * from itemdetails limit $start,$per_page");

?>
<body>
<?php
require 'view/menu-pc.php';


?>


<!-- Start Content -->
<div class="container-fluid fixed">


    <div id="content">
        <div class="heading-buttons">
            <h2>Dashboard<span>| Metalink.com</span></h2>

        </div>


        <div class="separator line"></div>


       <!-- item and category details view-->
        <div class="widget widget-4" style="margin-top: -1px;">
            <div class="widget-head">
                <h4 class="heading"><?php echo date('l jS \of F Y');?></h4>
            </div>
            <div class="widget-body" style="padding: 10px 0;">
                <div class="separator bottom form-inline small">
                   <?php echo 'Total Items : '.mysql_result($pages_query,0)?>
			<span class="pull-right">
				<label class="strong">Sort by:</label>
				<select class="selectpicker" data-style="btn-default btn-small">
                    <option />Option
                    <option />Option
                    <option />Option
                </select>
			</span>
                </div>
                <table class="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs">
                    <thead>
                    <tr>

                        <th style="width: 1%;" class="uniformjs"><input type="checkbox" /></th>
                        <th class="center">Name</th>
                        <th class="center">Category</th>
                        <th>Model</th>
                        <th class="center">Productivity</th>
                        <!--th class="center">Size</th-->
                        <th class="center">Company</th>
                        <!--th class="center">Description</th-->
                        <th class="center">Manufacture</th>
                        <th class="center">Price</th>
                        <th class="center" style="width: 90px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row=mysql_fetch_assoc($colors))
                    {

                        //echo '<td class="center uniformjs"><input type="checkbox" /></td>';

                        echo '<tr class="selectable dbtable">';
                        echo '<td class="center uniformjs"><input type="checkbox" /></td>';
                        echo ' <td class="center dbtable">'.$row['item_name'].'</td>';
                        echo '<td class="center dbtable">'.$row['item_cat'].'</td>';
                        echo '<td>'.$row['item_modelNo'].'</td>';
                        echo '<td class="center">'.$row['item_prdtivity'].'</td>';
                        //echo ' <td class="center">'.$row['item_size'].'</td>';
                        echo ' <td class="center">'.$row['item_company'].'</td>';
                       // echo ' <td class="center">'.$row['item_desc'].'</td>';
                        echo ' <td class="center">'.$row['item_manufacture'].'</td>';
                        echo ' <td class="center">'.$row['item_price'].'</td>';
                        echo ' <td class="center">';
                        echo '<a href="#" class="btn-action glyphicons eye_open btn-info"><i></i></a>';
                        echo '<a href="admin_item.php?itm='.$row['item_code'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
                        echo '<a href="#" class="btn-action glyphicons remove_2 btn-danger"><i></i></a>';
                        echo '</td>';
                        echo '</tr>';

                    }

                    ?>


                    </tbody>
                </table>
                <div class="separator top form-inline small">
                    <div class="pull-left checkboxs_actions hide">
                        <label class="strong">With selected:</label>
                        <select class="selectpicker" data-style="btn-default btn-small">
                            <option />Action
                            <option />Action
                            <option />Action
                        </select>
                    </div>
                    <div class="pagination pagination-small pull-right" style="margin: 0;">
                        <ul>
                            <?php
                            for($number=1;$number<=$pages;$number++)
                            { echo "<a href=?page=$number>".$number."</a>";

                               echo "<li class=active><a href=?page=$number>".$number."</a></li>";

                            }
                            ?>

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="separator line"></div>


    </div>

</div>

<?php

require 'view/footer.php';
?>

</body>
</html>
<div class="navbar main">

    <div class="container">
        <div class="row">
            <div class="span12 relativeWrap">

                <button type="button" class="btn btn-navbar visible-phone">
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>

                <ul id="menu" class="hidden-phone">
                    <li class="active">
                        <a href="admin.php" class="menuToggle">ADMIN</a>
                    </li>
                    <li >
                        <a href="admin_category.php" class="menuToggle">CATEGORY</a>
                    </li>
                    <li >
                        <a href="admin_item.php" class="menuToggle">ITEM</a>
                    </li>



                </ul>

					<span class="profile">
						<span>
							<strong></strong>
							<a href="index.php">log out</a>
						</span>
						
					</span>

            </div>
        </div>
    </div>
</div>
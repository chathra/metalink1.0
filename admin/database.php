<?php
/**
 * Created by PhpStorm.
 * User: {}
 * Date: 5/24/2015
 * Time: 8:26 PM
 */
require_once('config.php');
class Database{
    private $connection;
    public $last_query;
    private $magic_quotes_active;
    private $real_escape_string_exists;

    function __construct()
    {
        $this->open();
    }

    function open(){
        $this->connection=mysql_connect('localhost','root','admin');
        if(!$this->connection) {
            echo "Database connection failed".mysql_error();
        }else{
            $select_db=mysql_select_db('metalinklive',$this->connection);
            if(!$select_db)
            {
                echo "Database selection failed";
            }
        }
    }

    function close()
    {
        if(isset($this->connection))
        {
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    public function query($sql)
    {
        $this->last_query=$sql;
        $result=mysql_query($sql);
        $this->confirm_query($result);
        return $result;
    }

    public function confirm_query($result){
        if(!$result)
        {
            $output = "Database query failed".mysql_error()."<br>";
            $output.= $this->last_query;
            die($output);

        }
    }

    public function fetch_array($result_set)
    {
        return mysql_fetch_array($result_set);
    }

    public function num_rows($result_set)
    {
        return mysql_num_rows($result_set);
    }

    public function mysql_prep( $value ) {
        $magic_quotes_active = get_magic_quotes_gpc();
        $new_enough_php = function_exists( "mysql_real_escape_string" );
        if( $new_enough_php ) {
            if( $magic_quotes_active ) { $value = stripslashes( $value ); }
            $value = mysql_real_escape_string( $value );
        } else {
            if( !$magic_quotes_active ) { $value = addslashes( $value ); }

        }
        return $value;
    }

    public function escape_value( $value )
    {
        if ($this->real_escape_string_exists) {
            if ($this->magic_quotes_active) {
                $value = stripslashes($value);
            }
            $value = mysql_real_escape_string($value);
        } else {
            if (!$this->magic_quotes_active) {
                $value = addslashes($value);
            }
                }
        return $value;
    }

    public function insert_id()
    {
        return mysql_insert_id($this->connection);
    }

    public function affected_rows()
    {
        return mysql_affected_rows($this->connection);
    }


}

$db=new Database();
<?php
require 'view/header.php';
require('db_object.php');
require('category.php');
require('initialize.php');
if(isset($_POST['submit']))
{
    $cat=new Category();

    $category_name= $cat->catgry_name=trim($_POST['category_name']);
    $category_key= $cat->catgry_code=trim($_POST['category_key']);
    if($cat->is_exists($category_name,$category_key))
    {
        echo "<script type='text/javascript'> alert('Category name and key exists !'); </script>";

    }else {
        if ($cat->create()) {
            echo "saved";
            redirect_to('admin_category.php');
        } else {
            echo $db->last_query;
        }
    }
}

?>

<body>
<?php
require 'view/menu-pc.php';
?>
<div class="container-fluid fixed">


    <div id="content">
        <ul class="breadcrumb">
            <li><a href="index.html" class="glyphicons home"><i></i> AIR</a></li>
            <li class="divider"></li>
            <li>Forms</li>
            <li class="divider"></li>
            <li>Demo Forms</li>
            <div class="widget widget-2 widget-tabs widget-tabs-2 tabs-right border-bottom-none">
                <div class="widget-head">
                    <ul>
                        <li class="active"><a class="glyphicons settings" href="#account-settings"
                                              data-toggle="tab"><i></i>Add News Category</a></li>

                    </ul>
                </div>
            </div>

            <form class="form-horizontal" method="post" action='admin_category.php' >
                <div class="tab-pane active" id="account-settings">
                    <div class="row-fluid">
                        <div class="span3">
                            <strong>Add New Category</strong>
                            <p class="muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        <div class="span9">

                            <div class="separator"></div>

                            <label for="inputPasswordOld">Category Name</label>
                            <input  type="text" id="categoryName" class="span10" name="category_name"
                                    placeholder="Leave empty for no change" required>

                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                            <div class="separator"></div>

                            <label for="inputPasswordOld">Category Key</label>
                            <input type="text" id="categoryKey" required name="category_key" class="span10" value=""
                                   placeholder="Leave empty for no change"/>
                        <span style="margin: 0;" class="btn-action single glyphicons circle_question_mark"
                              data-toggle="tooltip" data-placement="top"
                              data-original-title="Leave empty if you don't wish to change the password"><i></i></span>

                            <div class="separator"></div>


                        </div>
                    </div>
                    <hr class="separator line"/>
                </div>
                <div class="form-actions" style="margin: 0; padding-right: 0;">
                    <button type="submit" name="submit" id="submit" class="btn btn-icon btn-primary glyphicons circle_ok pull-right" >

                        <i></i>Save
                        changes
                    </button>
                </div>

    </div>
</div>
</form>
</div>
<?php

require 'view/footer.php';
?>

</body>

<!-- script area -->
<script type="text/javascript">

    $('form.ajax').on('submit', function (e) {
        e.preventDefault();
        console.log("it is word");
    });



    }</script>

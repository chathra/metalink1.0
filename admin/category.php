<?php
/**
 * Created by PhpStorm.
 * User: {}
 * Date: 5/24/2015
 * Time: 10:59 PM
 */
require_once('db_object.php');
class Category extends Db_object
{
    public   $ctgry_id;
    public $catgry_name;
    public $catgry_code;
    public $catgry_date;
    public $categry_state;
    public $categry_lstUpdate;
    protected static $table='itemcategory';
    public  static $db_fields=array('ctgry_id','catgry_name','catgry_code','catgry_date','categry_state','categry_lstUpdate');

    function __construct()
    {
        parent::$id=$this->ctgry_id;
        $this->categry_state=1;
        $this->catgry_date=strftime("%Y-%m-%d %H:%M:%S",time());
        $this->categry_lstUpdate=strftime("%Y-%m-%d %H:%M:%S",time());
    }

    function is_exists($catgry_name,$catgry_code)
    {
        $result= self::find_by_sql("select * from ".self::$table." where catgry_name='$catgry_name' and catgry_code='$catgry_code'");
        if($result)
        {
            return true;
        }else{return false;}
    }

}
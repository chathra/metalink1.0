-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2015 at 08:29 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `metalinklive`
--

-- --------------------------------------------------------

--
-- Table structure for table `itemdetails`
--

DROP TABLE IF EXISTS `itemdetails`;
CREATE TABLE IF NOT EXISTS `itemdetails` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(150) NOT NULL,
  `item_cat` varchar(20) NOT NULL,
  `item_code` varchar(50) DEFAULT NULL,
  `item_modelNo` varchar(100) DEFAULT NULL,
  `item_prdtivity` varchar(100) DEFAULT NULL,
  `item_size` varchar(100) DEFAULT NULL,
  `item_company` varchar(100) DEFAULT NULL,
  `item_desc` text,
  `item_manufacture` varchar(100) DEFAULT NULL,
  `item_price` float DEFAULT NULL,
  `item_lstUpdate` datetime DEFAULT NULL,
  `item_date` datetime DEFAULT NULL,
  `item_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `itemdetails`
--

INSERT INTO `itemdetails` (`item_id`, `item_name`, `item_cat`, `item_code`, `item_modelNo`, `item_prdtivity`, `item_size`, `item_company`, `item_desc`, `item_manufacture`, `item_price`, `item_lstUpdate`, `item_date`, `item_state`) VALUES
(53, 'Makita 156 MX-VL', 'door_lock', 'Makita.156.MX-VL', 'SH-P01', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '2015-06-02 07:23:04', '0000-00-00 00:00:00', 1),
(57, 'Bosch XC', 'door_lock', 'Bosch.XC', 'Bosch XC', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:22:05', 1),
(58, 'Makita 156 MX-VL', 'padlock', 'padl1', 'padl1', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:30:33', 1),
(59, 'Lotus PP4', 'padlock', 'Lotus.PP4', 'Lotus PP4', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:32:28', 1),
(60, 'Lotus PP4', 'door_handle', 'dh1', 'dh1', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:37:30', 1),
(61, 'hinger 12', 'hinges', 'hin1', 'hin1', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:38:25', 1),
(62, 'pull x12', 'door_pull', 'pull1', 'pull1', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:42:07', 1),
(63, 'scale x45', 'scales', 'scale1', 'scale1', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:43:09', 1),
(64, 'Grt c32', 'cut_and_grinding', 'catg1', 'catg1', '10,0000 Pcs/Year', '102 x 96 x 3mm, 102 x 106 x 3mm, 127 x 96 x 3mm', 'Foshan Yaojin Hardwares Co., Ltd.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Foshan Yaojin Hardwares Co., Ltd.', 0, '0000-00-00 00:00:00', '2015-06-02 07:44:22', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

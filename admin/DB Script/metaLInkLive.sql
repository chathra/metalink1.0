/*
SQLyog Ultimate - MySQL GUI v8.2 
MySQL - 5.5.27 : Database - metalinklive
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`metalinklive` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `metalinklive`;

/*Table structure for table `itemcategory` */

DROP TABLE IF EXISTS `itemcategory`;

CREATE TABLE `itemcategory` (
  `ctgry_id` int(11) NOT NULL AUTO_INCREMENT,
  `catgry_name` varchar(150) NOT NULL,
  `catgry_code` varchar(50) DEFAULT NULL,
  `catgry_date` datetime NOT NULL,
  `categry_state` int(11) NOT NULL COMMENT '0 or 1',
  `categry_lstUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`ctgry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `itemcategory` */

/*Table structure for table `itemdetails` */

DROP TABLE IF EXISTS `itemdetails`;

CREATE TABLE `itemdetails` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(150) NOT NULL,
  `item_code` varchar(50) DEFAULT NULL,
  `item_modelNo` varchar(100) DEFAULT NULL,
  `item_prdtivity` varchar(100) DEFAULT NULL,
  `item_size` varchar(100) DEFAULT NULL,
  `item_company` varchar(100) DEFAULT NULL,
  `item_desc` varchar(200) DEFAULT NULL,
  `item_manufacture` varchar(100) DEFAULT NULL,
  `item_price` float DEFAULT NULL,
  `item_lstUpdate` datetime DEFAULT NULL,
  `item_date` datetime DEFAULT NULL,
  `item_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `itemdetails` */

/*Table structure for table `itemimage` */

DROP TABLE IF EXISTS `itemimage`;

CREATE TABLE `itemimage` (
  `itemGry_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `image_date` datetime DEFAULT NULL,
  PRIMARY KEY (`itemGry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `itemimage` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

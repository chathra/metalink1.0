-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2015 at 07:00 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `metalinklive`
--

-- --------------------------------------------------------

--
-- Table structure for table `itemcategory`
--

CREATE TABLE IF NOT EXISTS `itemcategory` (
  `ctgry_id` int(11) NOT NULL AUTO_INCREMENT,
  `catgry_name` varchar(150) NOT NULL,
  `catgry_code` varchar(50) DEFAULT NULL,
  `catgry_date` datetime NOT NULL,
  `categry_state` int(11) NOT NULL COMMENT '0 or 1',
  `categry_lstUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`ctgry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `itemcategory`
--

INSERT INTO `itemcategory` (`ctgry_id`, `catgry_name`, `catgry_code`, `catgry_date`, `categry_state`, `categry_lstUpdate`) VALUES
(35, 'category1', 'cat1', '2015-05-27 12:42:27', 1, '2015-05-27 12:42:27'),
(36, 'category2', 'cat2', '2015-05-27 12:42:41', 1, '2015-05-27 12:42:41'),
(37, 'category4', 'cat4', '2015-05-30 16:09:57', 1, '2015-05-30 16:09:57'),
(38, 'category5', 'cat5', '2015-05-30 16:25:26', 1, '2015-05-30 16:25:26');

-- --------------------------------------------------------

--
-- Table structure for table `itemdetails`
--

CREATE TABLE IF NOT EXISTS `itemdetails` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(150) NOT NULL,
  `item_cat` varchar(20) NOT NULL,
  `item_code` varchar(50) DEFAULT NULL,
  `item_modelNo` varchar(100) DEFAULT NULL,
  `item_prdtivity` varchar(100) DEFAULT NULL,
  `item_size` varchar(100) DEFAULT NULL,
  `item_company` varchar(100) DEFAULT NULL,
  `item_desc` text,
  `item_manufacture` varchar(100) DEFAULT NULL,
  `item_price` float DEFAULT NULL,
  `item_lstUpdate` datetime DEFAULT NULL,
  `item_date` datetime DEFAULT NULL,
  `item_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `itemdetails`
--

INSERT INTO `itemdetails` (`item_id`, `item_name`, `item_cat`, `item_code`, `item_modelNo`, `item_prdtivity`, `item_size`, `item_company`, `item_desc`, `item_manufacture`, `item_price`, `item_lstUpdate`, `item_date`, `item_state`) VALUES
(0, 'item1up2', 'category1', 'itemcode1', 'model1', '', '', '', '', '', 1, '2015-05-30 15:59:41', '0000-00-00 00:00:00', 1),
(45, 'item2up2', 'category2', 'itemcat2', 'model2', '', '', '', '', '', 2, '2015-05-30 18:45:58', '0000-00-00 00:00:00', 1),
(46, 'item4up', 'category4', 'code4', '', '', '', '', '', '', 4, '2015-05-30 18:33:05', '0000-00-00 00:00:00', 1),
(47, 'item5', 'category5', 'code5', '', '', '', '', '', '', 5, '2015-05-30 16:27:41', '0000-00-00 00:00:00', 1),
(48, 'item11', 'category2', 'code11', '', '', '', '', '', '', 0, '0000-00-00 00:00:00', '2015-05-30 18:35:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `itemimage`
--

CREATE TABLE IF NOT EXISTS `itemimage` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_code` varchar(255) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `image_date` datetime DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `itemimage`
--

INSERT INTO `itemimage` (`image_id`, `item_code`, `image_name`, `image_date`) VALUES
(0, 'itemcat2', 'itemcat2', '2015-05-30 18:46:11'),
(65, 'itemcode1', 'itemcode1', '2015-05-27 12:43:28'),
(66, 'code4', 'code4', '2015-05-30 18:35:03'),
(67, 'code5', 'code5', '2015-05-30 16:27:47'),
(68, '', 'code11', '2015-05-30 18:35:44');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `password`, `last_login`) VALUES
(1, 'adminx', 'aDmin@admi', '2015-05-30 22:15:47');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

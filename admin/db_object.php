<?php
/**
 * Created by PhpStorm.
 * User: {}
 * Date: 5/24/2015
 * Time: 10:54 PM
 */
require_once('database.php');

class Db_object
{

    public static $db_fields = array();
//    protected static $table="";
    public static $id = "";

    public static function find_all()
    {

        return self::find_by_sql("select * from " . static::$table);

    }

    //modification user damith
    // remove limit 1
    public static function find_by_id($field, $id = 0)
    {
        global $db;
        $result_array = self::find_by_sql("select * from " . static::$table . " WHERE $field='$id'");

        return !empty($result_array) ? array_shift($result_array) : false;
    }


    public static function find_by_sql($sql = "")
    {
        global $db;
        $result_set = $db->query($sql);
        $object_array = array();
        while ($row = $db->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    private static function instantiate($record)
    {
        $object = new static;
//        $object->id      =$record['id'];
//        $object->user_name         =$record['user_name'];
//        $object->password         =$record['password'];
//        $object->fname         =$record['first_name'];
//        $object->lname         =$record['last_name'];
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute)
    {
        $object_vars = $this->attributes();
        return array_key_exists($attribute, $object_vars);
    }

//    protected function attribute()
//    {
//        $attributes=array();
//        foreach(static::$db_fields as $field)
//        {
//            if(property_exists($this,$field))
//            {
//                $attributes[$field]=$this->$field;
//            }
//        }
//        return  $attributes;
//    }

    protected function attributes()
    {
        $attributes = array();
        foreach (static::$db_fields as $field) {
            if (property_exists(get_called_class(), $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes()
    {
        global $db;
        $clean_attributes = array();
        foreach (self::attributes() as $key => $values) {
            $clean_attributes[$key] = $db->escape_value($values);
        }
        return $clean_attributes;
    }


    public function save()
    {
        return (isset($this->id)) ? $this->update() : $this->create();
    }

    public function create()
    {

        global $db;
        $attributes = self::attributes();
        array_shift($attributes);
        //$sql="insert into ".self::$table." (user_name,password,first_name,last_name) VALUES ('$this->user_name','$this->password','$this->first_name','$this->last_name')";
        $sql = "INSERT INTO " . static::$table . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if ($db->query($sql)) {
            self::$id = $db->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update($field, $data)
    {
        global $db;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "update " . static::$table . " set ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " where $field='$data'";
        $db->query($sql);
        return ($db->affected_rows() == 1) ? true : false;
    }

    public function delete()
    {
        global $db;
        $sql = "delete from " . static::$table . " WHERE id='$this->id' limit 1";
        $db->query($sql);
        return ($db->affected_rows() == 1) ? true : false;
    }

    public static function test()
    {
        echo static::$table;
    }

    // User damith
    // Date 6/2/2015
    // get_num_row($field, $id = 0)

    // get num of row
    // param {$field, $id = 0}
    //return int
    public static function get_num_row($field, $id = 0)
    {
        global $db;
        $sql = "select * from " . static::$table . " WHERE $field='$id'";
        $result_set = $db->query($sql);
        $num_row = $db->num_rows($result_set);
        return ($num_row > 0) ? $num_row : null;
    }

}
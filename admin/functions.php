<?php
/**
 * Created by PhpStorm.
 * User: {}
 * Date: 5/20/2015
 * Time: 4:06 PM
 */
function redirect_to($location=null)
{
    if ($location!=null)
    {
        header("Location:{$location}");
        exit;
    }
}
function output_message($message="")
{
    if(!empty($message))
    {
        return "<p classs=\"message\">{$message}</p>";
    }else{
        return "";
    }
}
function __autoload($class_name)

{
    $class_name=strtolower($class_name);
    $path="$class_name.php";
    if(file_exists($path))
    {
        require_once($path);
    }else{
        die("The file $class_name.php could not be found");

    }
}
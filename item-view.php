<!-- top header -->
<?php
require 'view/top-header.php';
require_once('admin/category.php');
require_once('admin/item.php');
require_once('admin/image.php');
?>

<body class="cnt-homepage">


<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">

    <!-- ============================================== TOP MENU ============================================== -->
    <?php
    require 'view/top-header-content.php';
    // menu PC
    require 'view/menu-pc.php';
    ?>

</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
            <ul class="list-inline list-unstyled" ng-controller="queryController as url">
                <li><a href="#">{{url.data.page}}</a></li>
                <li><a>{{url.data.category}}</a></li>
                <li class='active'>{{url.data.name}}</li>
            </ul>
        </div>
        <!-- /.breadcrumb-inner -->
    </div>
    <!-- /.container -->
</div>
<!-- /.breadcrumb -->
<!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
<div class='container'>
<div class="homepage-container">
<div class='row single-product outer-bottom-sm '>
<div class='col-md-3 sidebar'>
    <div class="sidebar-module-container">
        <!-- ============================================== RELATED PRODUCTS ============================================== -->
        <div class="sidebar-widget hot-deals wow fadeInUp">
            <h3 class="section-title">hot deals</h3>

            <!--  hot detials -->
            <div class="owl-carousel related-product sidebar-carousel custom-carousel owl-theme outer-top-xs">

                <!-- /.hot-deal-wrapper -->
                <?php
                $queryID = $_GET['id'];
                $sql = "select item_id,item_name,item_cat,item_code,item_modelNo,item_size,
                         item_desc,item_manufacture,item_price from itemdetails where item_state=1 AND item_id NOT LIKE ' $queryID'  ORDER BY item_id DESC LIMIT 3 ";
                $item_array = Item::find_by_sql($sql);
                // $cat_arry = Category::find_all();
                foreach ($item_array as $item):

                    // Get related category
                    $sql = "select catgry_name from itemcategory where ctgry_id= '" . $item->item_cat . "' limit 1";
                    $cat_ary = Category::find_by_sql($sql);
                    foreach ($cat_ary as $category):
                        $categoryName = $category->catgry_name;
                        $category = null;
                    endforeach;

                    // Get related image
                    $sql = "select image_id,image_name from itemimage where item_code= '" . $item->item_code . "' limit 1";
                    $item_image_array = Image::find_by_sql($sql);
                    foreach ($item_image_array as $image):
                        $imageName = $image->image_name;
                    endforeach;
                    ?>
                    <div class="item">
                        <div class="products">
                            <div class="hot-deal-wrapper">
                                <div class="image">
                                    <img src="img/<?php
                                    if (!file_exists('img/' . $imageName)) {
                                        echo "not found";
                                    } else {
                                        echo $imageName;
                                    }
                                    $image = null;
                                    ?>  " width="263" height="350"/>
                                </div>
                            </div>
                            <div class="product-info text-left m-t-20">
                                <h3 class="name"><a
                                        href="indexbd17.html?page=detail"><?php echo $item->item_name; ?></a></h3>

                                <div class="rating ">
                                    <?php
                                    echo $categoryName;
                                    ?>
                                </div>
                                <div class="product-price">
								<span class="price">
									Rs <?php
                                    echo number_format($item->item_price, 2);
                                    ?>
								</span>
                                </div>
                                <!-- /.product-price -->
                            </div>
                            <!-- /.product-info -->
                            <div class="action"><a class="lnk btn btn-primary"
                                                   href="item-view.php?name=<?php echo $item->item_name; ?>&id=<?php echo $item->item_code; ?>&cat=<?php echo $categoryName; ?>">View
                                    More</a></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- end details -->
            <!-- /.sidebar-widget -->
        </div>
        <!-- ============================================== RELATED PRODUCTS: END ============================================== -->

        <!-- ============================================== COLOR: END ============================================== -->

    </div>
</div>
<!-- /.sidebar -->
<div class='col-md-9'>
    <div class="row  wow fadeInUp">
        <div class="col-xs-12 col-sm-6 col-md-7 gallery-holder">
            <div class="product-item-holder size-big single-product-gallery small-gallery">

                <div id="owl-single-product">
                    <div class="single-product-gallery-item" id="slide1">
                        <a data-lightbox="image-1" data-title="Gallery" href="assets/images/fashion-products/d1.jpg">

                            <?php
                            // Get related image
                            $sql = "select image_id,image_name from itemimage where item_code= '" . $queryID . "' limit 1";
                            $item_image_array = Image::find_by_sql($sql);
                            foreach ($item_image_array as $image):
                                $imageName = $image->image_name;
                            endforeach;
                            ?>
                            <img src="img/<?php
                            if (!file_exists('img/' . $imageName)) {
                                echo "not found";
                            } else {
                                echo $imageName;
                            }
                            $image = null;
                            ?>  " width="480" height="580"/>
                        </a>
                    </div>
                    <!-- /.single-product-gallery-item -->
                </div>
                <!-- /.single-product-slider -->
            </div>
            <!-- /.single-product-gallery -->
        </div>
        <!-- /.gallery-holder -->
        <div class='col-sm-6 col-md-5 product-info-block'>
            <div class="product-info">
                <h1 class="name"><?php echo $item->item_name; ?></h1>

                <div class="rating-reviews m-t-20">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="rating ">Manufacture:</div>
                        </div>
                        <div class="col-sm-8">
                            <div class="reviews">
                                <a href="#" class="lnk"><?php echo $item->item_manufacture ?></a>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.rating-reviews -->

                <div class="stock-container info-container m-t-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="stock-box">
                                <span class="label">Availability :</span>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="stock-box">
                                <span class="value">Instock</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.stock-container -->

                <div class="item">
                    <div class="product-info text-left m-t-20">
                        <div class="rating ">
                            <span class="label">
                            Size : </span><?php echo $item->item_size; ?>
                        </div>
                        <div class="rating ">
                            Model No : <?php echo $item->item_modelNo; ?>
                            </br>
                        </div>

                        <div class="product-price">
                        <span class="price">
                            Price : Rs <?php
                            echo number_format($item->item_price, 2);
                            ?>
                            </span>
                        </div>


                        <!-- /.product-price -->
                    </div>
                    <!-- /.prod -->
                </div>


                <div class="description-container m-t-20">
                    <?php
                    echo $item->item_desc;
                    ?>
                </div>
                <!-- /.description-container -->


            </div>
            <!-- /.product-info -->
        </div>
        <!-- /.col-sm-5 -->
    </div>
    <!-- /.row -->
    <div class="product-tabs outer-top-smal  wow fadeInUp">

        <ul id="product-tabs" class="nav nav-tabs nav-tab-cell-detail">
            <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
        </ul>
        <!-- /.nav-tabs #product-tabs -->


        <div class="tab-content outer-top-xs">

            <div id="description" class="tab-pane in active">
                <div class="product-tab">
                    <p class="text">
                        <?php
                        echo $item->item_desc;
                        ?>

                    <p>
                </div>
            </div>
            <!-- /.tab-pane -->


            <!-- /.tab-pane -->

        </div>
        <!-- /.tab-content -->

    </div>
    <!-- /.product-tabs -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->


<!-- ============================================== FEATURED PRODUCT ============================================== -->

<!-- /.section -->
<!-- ============================================== FEATURED PRODUCT : END ============================================== -->


<div class="clearfix"></div>

<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
</div>
<!-- /.homepage-container -->
</div>
<!-- /.container -->
</div>
<!-- /.body-content -->

<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
    <?php
    require 'view/footer.php';
    ?>

</footer>
<!-- ============================================================= FOOTER : END============================================================= -->

<?php
include 'view/script.php';
?>
s
<script type="text/javascript" src="js/itemView.js"></script>

</body>

<!-- Mirrored from transvelo.in/demo/html/unicase/index.php?page=detail2 by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 31 May 2015 17:02:55 GMT -->
</html>
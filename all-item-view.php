<!-- top header -->
<?php
require_once('admin/category.php');
$cat = Category::find_all();
?>
<?php
require 'view/top-header.php';
require_once('admin/category.php');
?>
<body class="cnt-home">


<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">
    <?php
    require 'view/top-header-content.php';

    require 'view/menu-pc.php';
    ?>
    <!-- ============================================== TOP MENU ============================================== -->

    <!-- ============================================== TOP MENU : END ============================================== -->

    <!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

</header>

<!-- ============================================== HEADER : END ============================================== -->
<div class="body-content outer-top-xs" id="top-banner-and-menu">
<div class="container">
<div class="furniture-container homepage-container">
<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-3 sidebar">
        <!-- ================================== TOP NAVIGATION ================================== -->
        <?php
        require 'view/main-category-panel.php'
        ?>
        <!-- /.side-menu -->
        <!-- ================================== TOP NAVIGATION : END ================================== -->
    </div>
    <!-- /.sidemenu-holder -->

    <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
        <!-- ========================================== SECTION – HERO ========================================= -->

        <div id="hero" class="homepage-slider3">
            <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                <div class="full-width-slider">
                    <div class="item" style="background-image: url(assets/images/sliders/fur1.jpg);">
                        <div class="container-fluid">
                            <div class="caption vertical-center text-left">
                                <div class="small fadeInDown-1">
                                    italia trend
                                </div>

                                <div class="big-text fadeInDown-2">
                                    open minded design
                                </div>

                                <div class="excerpt fadeInDown-3 hidden-xs">created for customers with modern,active and
                                    easy lifestyles ,this collection features sleek,innovative designs envisioned in the
                                    style center of italy.
                                </div>
                            </div>
                            <!-- /.caption -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.item -->
                </div>
                <!-- /.full-width-slider -->

                <div class="full-width-slider">
                    <div class="item full-width-slider" style="background-image: url(assets/images/sliders/fur1.jpg);">
                        <div class="container-fluid">
                            <div class="caption vertical-center text-left">
                                <div class="small fadeInDown-1">
                                    italia trend
                                </div>

                                <div class="big-text fadeInDown-2">
                                    open minded design
                                </div>

                                <div class="excerpt fadeInDown-3 hidden-xs">created for customers with modern,active and
                                    easy lifestyles ,this collection features sleek,innovative designs envisioned in the
                                    style center of italy.
                                </div>
                            </div>
                            <!-- /.caption -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.item -->
                </div>
                <!-- /.full-width-slider -->

            </div>
            <!-- /.owl-carousel -->
        </div>

        <!-- ========================================= SECTION – HERO : END ========================================= -->
        <!-- ============================================== INFO BOXES ============================================== -->
        <div class="info-boxes wow fadeInUp">
            <div class="info-boxes-inner">
                <div class="row">
                    <div class="col-md-6 col-sm-4 col-lg-4">
                        <div class="info-box">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="icon fa fa-dollar"></i>
                                </div>
                                <div class="col-xs-10">
                                    <h4 class="info-box-heading green">money back</h4>
                                </div>
                            </div>
                            <h6 class="text">30 Day Money Back Guarantee.</h6>
                        </div>
                    </div>
                    <!-- .col -->

                    <div class="hidden-md col-sm-4 col-lg-4">
                        <div class="info-box">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="icon fa fa-truck"></i>
                                </div>
                                <div class="col-xs-10">
                                    <h4 class="info-box-heading orange">free shipping</h4>
                                </div>
                            </div>
                            <h6 class="text">free ship-on oder over $600.00</h6>
                        </div>
                    </div>
                    <!-- .col -->

                    <div class="col-md-6 col-sm-4 col-lg-4">
                        <div class="info-box">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="icon fa fa-gift"></i>
                                </div>
                                <div class="col-xs-10">
                                    <h4 class="info-box-heading red">Special Sale</h4>
                                </div>
                            </div>
                            <h6 class="text">All items-sale up to 20% off </h6>
                        </div>
                    </div>
                    <!-- .col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.info-boxes-inner -->

        </div>
        <!-- /.info-boxes -->
        <!-- ============================================== INFO BOXES : END ============================================== -->
    </div>
    <!-- /.homebanner-holder -->

</div>
<!-- /.row -->

<!-- ============================================== SCROLL TABS ============================================== -->
<div class="tab-content outer-top-xs">
<!-- /.tab-pane -->
<div class="tab-pane" id="woman">
<div class="product-slider">
<div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/3.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag sale"><span>sale</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/4.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/2.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/5.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag sale"><span>sale</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/1.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag hot"><span>hot</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/6.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag new"><span>new</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->
</div>
<!-- /.home-owl-carousel -->
</div>
<!-- /.product-slider -->
</div>
<!-- /.tab-pane -->

<div class="tab-pane" id="man">
<div class="product-slider">
<div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/5.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag sale"><span>sale</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/4.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/1.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag hot"><span>hot</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/2.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/3.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag sale"><span>sale</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/6.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag new"><span>new</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->
</div>
<!-- /.home-owl-carousel -->
</div>
<!-- /.product-slider -->
</div>
<!-- /.tab-pane -->

<div class="tab-pane" id="kids">
<div class="product-slider">
<div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/5.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag sale"><span>sale</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/6.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag new"><span>new</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/2.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/4.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->


            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->

<div class="item item-carousel">
    <div class="products">

        <div class="product">
            <div class="product-image">
                <div class="image">
                    <a href="indexbd17.html?page=detail"><img src="assets/images/blank.gif"
                                                              data-echo="assets/images/furniture-products/1.jpg"
                                                              alt=""></a>
                </div>
                <!-- /.image -->

                <div class="tag hot"><span>hot</span></div>
            </div>
            <!-- /.product-image -->


            <div class="product-info text-left">
                <h3 class="name"><a href="indexbd17.html?page=detail">Simple Product Showcase</a></h3>

                <div class="rating rateit-small"></div>
                <div class="description"></div>

                <div class="product-price">
				<span class="price">
					$650.99				</span>
                    <span class="price-before-discount">$ 800</span>

                </div>
                <!-- /.product-price -->

            </div>
            <!-- /.product-info -->
            <div class="action"><a href="#" class="lnk btn btn-primary">Add to Cart</a></div>
        </div>
        <!-- /.product -->

    </div>
    <!-- /.products -->
</div>
<!-- /.item -->


<!-- /.item -->
</div>
<!-- /.home-owl-carousel -->
</div>
<!-- /.product-slider -->
</div>
<!-- /.tab-pane -->

</div>
<!-- /.tab-content -->
</div>
<!-- /.scroll-tabs -->
<!-- ============================================== SCROLL TABS : END ============================================== -->
<?php
require 'view/categories-view.php';
?>
<!-- ============================================== WIDE BANNERS ============================================== -->
<div class="wide-banners">
    <div class="row">
        <?php
         require  'view/page-banner-2x2.php';
        ?>
    </div>
</div>
<!-- ============================================== WIDE BANNERS : END ============================================== -->

<!-- ============================================== TABS ============================================== -->
<div class="sections prod-slider-small outer-top-small">
    <div class="row">
        <div class="col-md-6">
            <?php
            require 'view/new-product.php';
            ?>
        </div>
        <div class="col-md-6">
            <?php
            require 'view/most-populer-items.php';
            ?>
        </div>
    </div>
</div>
<!-- ============================================== TABS : END ============================================== -->

<!-- ============================================== BLOG ============================================== -->
<section class="section outer-top-small wow fadeInUp ">
    <h3 class="section-title">latest form blog</h3>

    <div class="blog-slider-container wow fadeInUp  outer-top-xs">
        <div class="owl-carousel blog-slider custom-carousel">
            <div class="item">
                <div class="blog-post">
                    <div class="blog-post-image">
                        <div class="image">
                            <img data-echo="assets/images/blog-post/fr1.jpg" src="assets/images/blank.gif" width="370"
                                 height="165" alt="">
                        </div>
                    </div>
                    <!-- /.blog-post-image -->


                    <div class="blog-post-info text-left">
                        <h3 class="name"><a href="#">Simple Blog demo from fashion web</a></h3>
                        <span class="info">By Jone Doe-22 april 2014 -03 comments</span>

                        <p class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                        <a href="#" class="lnk read-more-bottom btn btn-primary">Read more</a>
                    </div>
                    <!-- /.blog-post-info -->


                </div>
                <!-- /.blog-post -->
            </div>
            <!-- /.item -->
            <div class="item">
                <div class="blog-post">
                    <div class="blog-post-image">
                        <div class="image">
                            <img data-echo="assets/images/blog-post/fr2.jpg" src="assets/images/blank.gif" width="370"
                                 height="165" alt="">
                        </div>
                    </div>
                    <!-- /.blog-post-image -->


                    <div class="blog-post-info text-left">
                        <h3 class="name"><a href="#">Simple Blog demo from fashion web</a></h3>
                        <span class="info">By Jone Doe-22 april 2014 -03 comments</span>

                        <p class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                        <a href="#" class="lnk read-more-bottom btn btn-primary">Read more</a>
                    </div>
                    <!-- /.blog-post-info -->


                </div>
                <!-- /.blog-post -->
            </div>
            <!-- /.item -->
            <div class="item">
                <div class="blog-post">
                    <div class="blog-post-image">
                        <div class="image">
                            <img data-echo="assets/images/blog-post/fr3.jpg" src="assets/images/blank.gif" width="370"
                                 height="165" alt="">
                        </div>
                    </div>
                    <!-- /.blog-post-image -->


                    <div class="blog-post-info text-left">
                        <h3 class="name"><a href="#">Simple Blog demo from fashion web</a></h3>
                        <span class="info">By Jone Doe-22 april 2014 -03 comments</span>

                        <p class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                        <a href="#" class="lnk read-more-bottom btn btn-primary">Read more</a>
                    </div>
                    <!-- /.blog-post-info -->


                </div>
                <!-- /.blog-post -->
            </div>
            <!-- /.item -->
            <div class="item">
                <div class="blog-post">
                    <div class="blog-post-image">
                        <div class="image">
                            <img data-echo="assets/images/blog-post/fr4.jpg" src="assets/images/blank.gif" width="370"
                                 height="165" alt="">
                        </div>
                    </div>
                    <!-- /.blog-post-image -->


                    <div class="blog-post-info text-left">
                        <h3 class="name"><a href="#">Simple Blog demo from fashion web</a></h3>
                        <span class="info">By Jone Doe-22 april 2014 -03 comments</span>

                        <p class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                        <a href="#" class="lnk read-more-bottom btn btn-primary">Read more</a>
                    </div>
                    <!-- /.blog-post-info -->


                </div>
                <!-- /.blog-post -->
            </div>
            <!-- /.item -->
            <div class="item">
                <div class="blog-post">
                    <div class="blog-post-image">
                        <div class="image">
                            <img data-echo="assets/images/blog-post/fr5.jpg" src="assets/images/blank.gif" width="370"
                                 height="165" alt="">
                        </div>
                    </div>
                    <!-- /.blog-post-image -->


                    <div class="blog-post-info text-left">
                        <h3 class="name"><a href="#">Simple Blog demo from fashion web</a></h3>
                        <span class="info">By Jone Doe-22 april 2014 -03 comments</span>

                        <p class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                        <a href="#" class="lnk read-more-bottom btn btn-primary">Read more</a>
                    </div>
                    <!-- /.blog-post-info -->


                </div>
                <!-- /.blog-post -->
            </div>
            <!-- /.item -->
            <div class="item">
                <div class="blog-post">
                    <div class="blog-post-image">
                        <div class="image">
                            <img data-echo="assets/images/blog-post/fr6.jpg" src="assets/images/blank.gif" width="370"
                                 height="165" alt="">
                        </div>
                    </div>
                    <!-- /.blog-post-image -->


                    <div class="blog-post-info text-left">
                        <h3 class="name"><a href="#">Simple Blog demo from fashion web</a></h3>
                        <span class="info">By Jone Doe-22 april 2014 -03 comments</span>

                        <p class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                        <a href="#" class="lnk read-more-bottom btn btn-primary">Read more</a>
                    </div>
                    <!-- /.blog-post-info -->


                </div>
                <!-- /.blog-post -->
            </div>
            <!-- /.item -->

        </div>
        <!-- /.owl-carousel -->
    </div>
    <!-- /.blog-slider-container -->
</section>
<!-- /.section -->

<!-- ============================================== BLOC : END ============================================== -->
<!-- ============================================== FEATURED PRODUCT ============================================== -->


<!-- ============================================== FEATURED PRODUCT : END ============================================== -->
<!-- ============================================== BRANDS CAROUSEL ============================================== -->

<!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
</div>
<!-- /.furniture-container -->
</div>
<!-- /.container -->
</div>
<!-- /#top-banner-and-menu -->


<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
<div class="links-social inner-top-sm">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <!-- ============================================================= CONTACT INFO ============================================================= -->
                <div class="contact-info">
                    <div class="footer-logo">
                        <div class="logo">
                            <a href="index9ba3.html?page=home">

                                <img src="assets/images/logo.png" alt="">

                            </a>
                        </div>
                        <!-- /.logo -->

                    </div>
                    <!-- /.footer-logo -->

                    <div class="module-body m-t-20">
                        <p class="about-us"> Nam libero tempore, cum soluta nobis est ses eligendi optio cumque cum
                            soluta nobis est ses eligendi optio cumque.</p>

                        <div class="social-icons">

                            <a href="http://facebook.com/transvelo" class='active'><i
                                    class="icon fa fa-facebook"></i></a>
                            <a href="#"><i class="icon fa fa-twitter"></i></a>
                            <a href="#"><i class="icon fa fa-linkedin"></i></a>
                            <a href="#"><i class="icon fa fa-rss"></i></a>
                            <a href="#"><i class="icon fa fa-pinterest"></i></a>

                        </div>
                        <!-- /.social-icons -->
                    </div>
                    <!-- /.module-body -->

                </div>
                <!-- /.contact-info -->
                <!-- ============================================================= CONTACT INFO : END ============================================================= -->
            </div>
            <!-- /.col -->

            <div class="col-xs-12 col-sm-6 col-md-3">
                <!-- ============================================================= CONTACT TIMING============================================================= -->
                <div class="contact-timing">
                    <div class="module-heading">
                        <h4 class="module-title">opening time</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body outer-top-xs">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td>Monday-Friday:</td>
                                    <td class="pull-right">08.00 To 18.00</td>
                                </tr>
                                <tr>
                                    <td>Saturday:</td>
                                    <td class="pull-right">09.00 To 20.00</td>
                                </tr>
                                <tr>
                                    <td>Sunday:</td>
                                    <td class="pull-right">10.00 To 20.00</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                        <p class='contact-number'>Hot Line:(400)888 868 848</p>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.contact-timing -->
                <!-- ============================================================= CONTACT TIMING : END ============================================================= -->
            </div>
            <!-- /.col -->

            <div class="col-xs-12 col-sm-6 col-md-3">
                <!-- ============================================================= LATEST TWEET============================================================= -->
                <div class="latest-tweet">
                    <div class="module-heading">
                        <h4 class="module-title">latest tweet</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body outer-top-xs">
                        <div class="re-twitter">
                            <div class="comment media">
                                <div class='pull-left'>
                    <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                    </span>
                                </div>
                                <div class="media-body">
                                    <a href="#">@laurakalbag</a> As a result of your previous recommendation :) 
                    <span class="time">
                        12 hours ago
                    </span>
                                </div>
                            </div>

                        </div>
                        <div class="re-twitter">
                            <div class="comment media">
                                <div class='pull-left'>
                    <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                    </span>
                                </div>
                                <div class="media-body">
                                    <a href="#">@laurakalbag</a> As a result of your previous recommendation :) 
                    <span class="time">
                        12 hours ago
                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.contact-timing -->
                <!-- ============================================================= LATEST TWEET : END ============================================================= -->
            </div>
            <!-- /.col -->

            <div class="col-xs-12 col-sm-6 col-md-3">
                <!-- ============================================================= INFORMATION============================================================= -->
                <div class="contact-information">
                    <div class="module-heading">
                        <h4 class="module-title">information</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body outer-top-xs">
                        <ul class="toggle-footer" style="">
                            <li class="media">
                                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                    </span>
                                </div>
                                <div class="media-body">
                                    <p>868 Any Stress,Burala Casi,Picasa USA.</p>
                                </div>
                            </li>

                            <li class="media">
                                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                    </span>
                                </div>
                                <div class="media-body">
                                    <p>(400) 0888 888 888<br>(400) 888 888 888</p>
                                </div>
                            </li>

                            <li class="media">
                                <div class="pull-left">
                     <span class="icon fa-stack fa-lg">
                      <i class="fa fa-circle fa-stack-2x"></i>
                      <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                    </span>
                                </div>
                                <div class="media-body">
                                    <span><a href="#">Contact @Unicase.com</a></span><br>
                                    <span><a href="#">Sale @Unicase.com</a></span>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.contact-timing -->
                <!-- ============================================================= INFORMATION : END ============================================================= -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /.links-social -->

<div class="footer-bottom inner-bottom-sm">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="module-heading outer-bottom-xs">
                    <h4 class="module-title">category</h4>
                </div>
                <!-- /.module-heading -->

                <div class="module-body">
                    <ul class='list-unstyled'>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Returns</a></li>
                        <li><a href="#">Libero Sed rhoncus</a></li>
                        <li><a href="#">Venenatis augue tellus</a></li>
                        <li><a href="#">My Vouchers</a></li>
                    </ul>
                </div>
                <!-- /.module-body -->
            </div>
            <!-- /.col -->

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="module-heading outer-bottom-xs">
                    <h4 class="module-title">my account</h4>
                </div>
                <!-- /.module-heading -->

                <div class="module-body">
                    <ul class='list-unstyled'>
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Customer Service</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Site Map</a></li>
                        <li><a href="#">Search Terms</a></li>
                    </ul>
                </div>
                <!-- /.module-body -->
            </div>
            <!-- /.col -->

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="module-heading outer-bottom-xs">
                    <h4 class="module-title">our services</h4>
                </div>
                <!-- /.module-heading -->

                <div class="module-body">
                    <ul class='list-unstyled'>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Returns</a></li>
                        <li><a href="#">Libero Sed rhoncus</a></li>
                        <li><a href="#">Venenatis augue tellus</a></li>
                        <li><a href="#">My Vouchers</a></li>
                    </ul>
                </div>
                <!-- /.module-body -->
            </div>
            <!-- /.col -->

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="module-heading outer-bottom-xs">
                    <h4 class="module-title">help & support</h4>
                </div>
                <!-- /.module-heading -->

                <div class="module-body">
                    <ul class='list-unstyled'>
                        <li><a href="#">Knowledgebase</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Contact Support</a></li>
                        <li><a href="#">Marketplace Blog</a></li>
                        <li><a href="#">About Unicase</a></li>
                    </ul>
                </div>
                <!-- /.module-body -->
            </div>
        </div>
    </div>
</div>

<div class="copyright-bar">
    <div class="container">
        <div class="col-xs-12 col-sm-6 no-padding">
            <div class="copyright">
                Copyright © 2014
                <a href="index9ba3.html?page=home">Unicase Shop.</a>
                - All rights Reserved
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 no-padding">
            <div class="clearfix payment-methods">
                <ul>
                    <li><img src="assets/images/payments/1.png" alt=""></li>
                    <li><img src="assets/images/payments/2.png" alt=""></li>
                    <li><img src="assets/images/payments/3.png" alt=""></li>
                    <li><img src="assets/images/payments/4.png" alt=""></li>
                    <li><img src="assets/images/payments/5.png" alt=""></li>
                </ul>
            </div>
            <!-- /.payment-methods -->
        </div>
    </div>
</div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= -->


<!-- For demo purposes – can be removed on production -->

<div class="config open">
    <div class="config-options">
        <h4>Pages</h4>
        <ul class="list-unstyled animate-dropdown">
            <li class="dropdown">
                <button class="dropdown-toggle btn btn-primary btn-block" data-toggle="dropdown">View Pages</button>
                <ul class="dropdown-menu" role="menu">
                    <li role="presentation" class="dropdown-header">Home Pages</li>
                    <li><a href="index9ba3.html?page=home">Home</a></li>
                    <li><a href="index41b4.html?page=home2">Home II</a></li>
                    <li><a href="index19d1.html?page=home-furniture">Home Furniture</a></li>
                    <li><a href="indexa42b.html?page=homepage1">Home Page I</a></li>
                    <li><a href="index2426.html?page=homepage2">Home Page II</a></li>
                    <li class="divider"></li>
                    <li role="presentation" class="dropdown-header">Other Pages</li>
                    <li><a href="indexf6f2.html?page=blog">Blog</a></li>
                    <li><a href="indexaa69.html?page=blog-details">Blog Details</a></li>
                    <li><a href="indexe2f2.html?page=category">Category</a></li>
                    <li><a href="index7d8e.html?page=category-2">Category II</a></li>
                    <li><a href="index994a.html?page=checkout">Checkout</a></li>
                    <li><a href="index53a6.html?page=contact">Contact</a></li>
                    <li><a href="indexbd17.html?page=detail">Detail</a></li>
                    <li><a href="indexf290.html?page=detail2">Detail II</a></li>
                    <li><a href="index4fc0.html?page=shopping-cart">Shopping Cart Summary</a></li>

                </ul>
            </li>
        </ul>
        <h4>Header Styles</h4>
        <ul class="list-unstyled">
            <li><a href="index9ba3.html?page=home">Header 1</a></li>
            <li><a href="indexa42b.html?page=homepage1">Header 2</a></li>
            <li><a href="index19d1.html?page=home-furniture">Header 3</a></li>
        </ul>
        <h4>Layouts</h4>
        <ul class="list-unstyled">
            <li><a href="indexa42b.html?page=homepage1">Full Width</a></li>
            <li><a href="index2426.html?page=homepage2">Boxed</a></li>
        </ul>
        <h4>Colors</h4>
        <ul>
            <li><a class="changecolor green-text" href="#" title="Green color">Green</a></li>
            <li><a class="changecolor blue-text" href="#" title="Blue color">Blue</a></li>
            <li><a class="changecolor red-text" href="#" title="Red color">Red</a></li>
            <li><a class="changecolor orange-text" href="#" title="Orange color">Orange</a></li>
            <li><a class="changecolor dark-green-text" href="#" title="Darkgreen color">Dark Green</a></li>
        </ul>
    </div>
    <a class="show-theme-options" href="#"><i class="fa fa-wrench"></i></a>
</div>
<!-- For demo purposes – can be removed on production : End -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery-1.11.1.min.js"></script>

<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>

<script src="assets/js/echo.min.js"></script>
<script src="assets/js/jquery.easing-1.3.min.js"></script>
<script src="assets/js/bootstrap-slider.min.js"></script>
<script src="assets/js/jquery.rateit.min.js"></script>
<script type="text/javascript" src="assets/js/lightbox.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/scripts.js"></script>

<!-- For demo purposes – can be removed on production -->

<script src="switchstylesheet/switchstylesheet.js"></script>

<script>
    $(document).ready(function () {
        $(".changecolor").switchstylesheet({seperator: "color"});
        $('.show-theme-options').click(function () {
            $(this).parent().toggleClass('open');
            return false;
        });
    });

    $(window).bind("load", function () {
        $('.show-theme-options').delay(2000).trigger('click');
    });
</script>
<!-- For demo purposes – can be removed on production : End -->


</body>

<!-- Mirrored from transvelo.in/demo/html/unicase/index.php?page=home-furniture by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 31 May 2015 17:03:52 GMT -->
</html>